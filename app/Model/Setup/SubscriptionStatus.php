<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class SubscriptionStatus extends Model
{	
    protected $table = 'subscription_status';

    public function members(){
        return $this->hasMany('App\Model\Member\Main', 'role_id')
        ->select('id', 'name');
    }
    
}
