<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{	
    protected $table = 'district';

  
    public function depots(){
        return $this->hasMany('App\Model\Member\Main', 'depot_id')
        ->select('id', 'user_id', 'province_id', 'district_id', 'depot_id', 'branch_id', 'subscription_id', 'branch_owner_id', 'is_structure_active')
        ->with([
            'user:id,name,email,phone,uid', 
            'subscription:id,name', 
            //'branch', 
            'district', 
            'province'
        ]);
    }

    public function province(){
        return $this->belongsTo('App\Model\Setup\Province', 'province_id');
   }
   public function district(){
    return $this->belongsTo('App\Model\User\Main', 'district_id');
}

   public function members(){
    return $this->hasMany('App\Model\Member\Main', 'district_id');
}
   
}
