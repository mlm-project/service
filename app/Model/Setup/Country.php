<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{	
    protected $table = 'country';

    public function provinces(){
        return $this->hasMany('App\Model\Setup\Province', 'country_id');
    }
}
