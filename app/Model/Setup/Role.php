<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{	
    protected $table = 'role';

    public function members(){
        return $this->hasMany('App\Model\Member\Main', 'role_id');
    }
   
    public function subscriptions(){
        return $this->hasMany('App\Model\Subscription\Main', 'role_id');
    }

    
}
