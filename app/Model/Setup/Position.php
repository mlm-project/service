<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{	
    protected $table = 'position';

    public function activator(){
        return $this->belongsTo('App\Model\User\Main', 'activator_id');
    }

    public function members(){
        return $this->hasMany('App\Model\Member\Main', 'position_id');
    }

}
