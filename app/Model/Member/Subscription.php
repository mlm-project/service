<?php

namespace App\Model\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
   	//use SoftDeletes;
    protected $table = 'member_subscriptions';

    public function member(){
        return $this->belongsTo('App\Model\Member\Main', 'member_id')
        ->select('id', 'user_id', 'subscription_id', 'contract_expired')
        ->with([
            // 'user:id,name,phone', 
            //'subscription:id,name'
        ])
        ;
    }

    public function subscription(){
        return $this->belongsTo('App\Model\Subscription\Main', 'subscription_id')
        ->select('id', 'name', 'requirement', 'unit_price', 'min_purchase', 'role');
    }

    public function status(){
        return $this->belongsTo('App\Model\Setup\SubscriptionStatus', 'status_id')
        ->select('id', 'name', 'icon')
        ;
    }

    // public function district(){
    //     return $this->belongsTo('App\Model\Setup\District', 'district_id')
    //     ->select('id', 'name');
    // }

    // public function province(){
    //     return $this->belongsTo('App\Model\Setup\Province', 'province_id')
    //     ->select('id', 'name');
    // }
   
}
