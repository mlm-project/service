<?php

namespace App\Model\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepotRequest extends Model
{	
	//use SoftDeletes;
    protected $table = 'depot_request';

    public function branch(){
        return $this->belongsTo('App\Model\Member\Main', 'branch_id')
        ->select('id', 'user_id')
        ->with([
            'user:id,name,phone,email,uid'
        ]);
    }
    
    public function depot(){
        return $this->belongsTo('App\Model\Member\Main', 'depot_id')
        ->select('id', 'user_id', 'created_at')
        ->with([
            'user:id,name,phone,email,uid'
        ]);
    }

    // public function district(){
    //     return $this->belongsTo('App\Model\Setup\District', 'district_id')
    //     ->select('id', 'name', 'province_id')
    //     ->with([
    //         'province:id,name'
    //     ]);
    // }

    

}
