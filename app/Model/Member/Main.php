<?php

namespace App\Model\Member;

use Illuminate\Database\Eloquent\Model;

class Main extends Model
{
    //use SoftDeletes;
    protected $table = 'member';

    public function user()
    {
        return $this->belongsTo('App\Model\User\Main', 'user_id')
        ->select('id','name_kh', 'name', 'phone', 'email', 'uid','address', 'is_active','created_at'); 
    }
   
    public function sponsor()
    {
        return $this->belongsTo('App\Model\Member\Main', 'sponsor_id')
        ->select('id', 'user_id', 'sponsor_id')
        ->with([
            'user:id,uid,name,name_kh,phone,email,address',
                  ]);
    }
    public function refferal()
    {
        return $this->belongsTo('App\Model\Member\Main', 'refferal_id')
        ->select('id', 'user_id')
        ->with(['user:id,uid,name,name_kh,phone,email,address']);
    }
   
    public function subscriptions(){
        return $this->hasMany('App\Model\Member\Subscription', 'member_id')
        ->select('id', 'member_id', 'subscription_id', 'status_id', 'price', 'activated_at', 'expired_at' );
    } 
    public function activeSubscription(){
        return $this->hasOne('App\Model\Member\Subscription', 'member_id')
        ->select('id', 'member_id', 'subscription_id', 'price', 'activated_at', 'expired_at')
        ->where('status_id', 2)
        ->with([
            'subscription:id,name'
        ])
        ;
    }
   
    public function district(){
        return $this->belongsTo('App\Model\Setup\District', 'district_id')
        ->select('id', 'name');
    }
    public function branch(){
        return $this->belongsTo('App\Model\Setup\Province', 'branch_id')
        ->select('id', 'name');
    }
    public function depot(){
        return $this->belongsTo('App\Model\Setup\District', 'depot_id');
    }

    public function branchOwner(){
        return $this->belongsTo('App\Model\Member\Main', 'branch_owner_id')
        ->select('id', 'user_id', 'branch_id')
        ->with(['user:id,uid,name,phone,email']);
    }

    public function orders(){
        return $this->hasMany('App\Model\Order\Main', 'buyer_id');
    }
    public function order()
    {
        return $this->hasMany('App\Model\Order\Main', 'member_id');
    }

    public function purchases(){
        return $this->hasMany('App\Model\Purchase\Main', 'member_id')
        ->select('id', 'product')
        ->with([
           
            'product:id,kh_name,en_name'
        ]);
    }
    public function subscription(){
        return $this->belongsTo('App\Model\Subscription\Main', 'subscription_id')
        ->select('id', 'name', 'requirement', 'unit_price', 'min_purchase', 'role', 'icon')
       ;
    }
    public function province(){
        return $this->belongsTo('App\Model\Setup\Province', 'province_id')
        ->select('id', 'name');
    }
    public function roles(){
        return $this->belongsTo('App\Model\Setup\Role', 'role_id');
    }

    public function depotRequests(){
        return $this->hasMany('App\Model\Member\DepotRequest', 'depot_id');
    }

    public function balance(){
        return $this->hasOne('App\Model\Transaction\USD', 'member_id')
        ->select('id', 'member_id', 'balance as amount')
        ->orderBy('id', 'DESC')
        
        ;
    }
    
}
