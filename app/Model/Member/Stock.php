<?php

namespace App\Model\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{	
	//use SoftDeletes;
    protected $table = 'member_stock';

    public function member(){
        return $this->belongsTo('App\Model\Member\Main', 'member_id')
        ->select('id', 'name');
    }

    public function  package(){
        return $this->belongsTo('App\Model\Package\Main', 'package_id');
    }

}
