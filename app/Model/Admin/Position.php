<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{	
	use SoftDeletes;
    protected $table = 'admins_position';

    public function admins(){
        return $this->hasMany('App\Model\Admin\Main', 'position_id');
    }

}
