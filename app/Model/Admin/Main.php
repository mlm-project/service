<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'admin';

    public function position(){
        return $this->belongsTo('App\Model\Admin\Position', 'position_id');
    }

    public function user(){
        return $this->belongsTo('App\Model\User\Main', 'user_id')
        ->select('id', 'type_id', 'name', 'phone', 'email');
    }
    

}
