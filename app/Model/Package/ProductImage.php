<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{	
	use SoftDeletes;
    protected $table = 'product_images';

    public function product(){
        return $this->belongsTo('App\Model\Package\Product', 'product_id');
    }
}
