<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackagePrice extends Model
{	
	use SoftDeletes;
    protected $table = 'package_prices';

    public function  package(){
        return $this->belongsTo('App\Model\Package\Main', 'package_id');
    }
    public function  product(){
        return $this->belongsTo('App\Model\Package\Product', 'product_id');
    }

}
