<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageProduct extends Model
{	
	
    protected $table = 'packages_products';

    public function product(){
        return $this->belongsTo('App\Model\Package\Product', 'product_id')
        
        ;
    }

    public function  package(){
        return $this->belongsTo('App\Model\Package\Main', 'package_id');
    }
    public function  meta(){
        return $this->belongsTo('App\Model\Package\Main', 'package_id')
        ->select('id','kh_name', 'en_name', 'image')
        ;
    }

}
