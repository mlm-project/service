<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{	
	use SoftDeletes;
    protected $table = 'product';

 
    public function  category(){
        return $this->belongsTo('App\Model\Package\ProductCategory', 'category_id')
        ->select('id', 'name');
    }
    public function  prices(){
        return $this->hasMany('App\Model\Package\ProductPrice', 'product_id')
        ->select('id', 'product_id', 'from', 'to', 'price');
    }
    public function orders(){
        return $this->hasMany('App\Model\Order\Detail', 'package_id');
    }

    public function  info(){
        return $this->hasOne('App\Model\Package\PackageProduct', 'product_id')
        ->select('id', 'package_id', 'product_id', 'qty')
        ->with([
            'package:id,cost_price,selling_price,discount,qty,n_of_units,description', 
            //'product'
        ]);
        // ->whereHas('package', function($query){
        //     $query->where('is_single_product', 1); 
        // })
        
    }


    

    public function  packages(){
        return $this->hasMany('App\Model\Package\PackageProduct', 'product_id')
       ;
    }
   
}
