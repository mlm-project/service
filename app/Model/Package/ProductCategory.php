<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{	
	use SoftDeletes;
    protected $table = 'products_category';

   	public function  products(){
         return $this->hasMany('App\Model\Package\Product', 'category_id');
    }

}
