<?php

namespace App\MLM;
use Illuminate\Http\Request;
use JWTAuth;
use Carbon\Carbon;

use App\Model\Member\Main as Member;
use App\Model\Member\Main as Node;
use App\Model\Transaction\USD as USDTrx; //For calculating today PV. 

class Network{
    
  	public static function rootNode(){
        $user       = JWTAuth::parseToken()->authenticate();
        return Self::nodeInterface($user->member);
    }

    public static function getNodes(){
        //echo env('FILE_URL'); die; 

        $limit = 10; 

        $nodes = []; 
        $edges = [];


        //===========================================================================>> Level I
        $nodeId      =   intval(isset($_GET['nodeId'])?$_GET['nodeId']:0);
        
        //$page      =   intval(isset($_GET['nodeId'])?$_GET['page']:1);
        $dataLevel1 = Node::where('sponsor_id', $nodeId)
        ->whereHas('subscription')
        ->orderBy('id', 'asc')->paginate($limit);


        if(count($dataLevel1)){
            foreach($dataLevel1 as $rowLevel1){

                $nodes[] = Self::nodeInterface($rowLevel1);
                $edges[] = ['from'=>$nodeId, 'to'=>$rowLevel1->id];


                //===========================================================================>> Level II
                $dataLevel2 = Node::where('sponsor_id', $rowLevel1->id)
                ->whereHas('subscription')
                ->orderBy('id', 'asc')
                ->paginate($limit);


                if(count($dataLevel2)>0){
                    foreach($dataLevel2 as $rowLevel2){

                        $nodes[] = Self::nodeInterface($rowLevel2);
                        $edges[] = ['from'=>$rowLevel1->id, 'to'=>$rowLevel2->id];

                        //===========================================================================>> Level III
                        $dataLevel3 = Node::where('sponsor_id', $rowLevel2->id)
                        ->whereHas('subscription')
                        ->orderBy('id', 'asc')->paginate($limit);

                        if(count($dataLevel3)>0){
                            foreach($dataLevel3 as $rowLevel3){

                                $nodes[] = Self::nodeInterface($rowLevel3);
                                $edges[] = ['from'=>$rowLevel2->id, 'to'=>$rowLevel3->id];

                                //For level 3, we need to check for more people
                                $numOfRef = Node::where('sponsor_id', $rowLevel3->id)->count();
                                if($numOfRef>0){
                                    $nodes[] = [
                                        'id'=>$rowLevel3->id.'-0', 
                                        'label'=>'More', 
                                        'image'=> env('FILE_URL').'/'.'public/icon/users.png'
                                    ];
                                    $edges[] = ['from'=>$rowLevel3->id, 'to'=>$rowLevel3->id.'-0'];
                                }
                            }

                            //===============================>> More Member to display
                            if($dataLevel3->hasMorePages()){
                                $more = $dataLevel3->total()-$dataLevel3->perPage()*$dataLevel3->currentPage(); 
                                $moreNode = $rowLevel2->id.'-'.$dataLevel3->currentPage();
                                $nodes[] = [
                                    'id'=>$moreNode, 
                                    'label'=>'More '.$more, 
                                    'image'=> env('FILE_URL').'/'.'public/icon/users.png'
                                ];
                                $edges[] = ['from'=>$rowLevel2->id, 'to'=>$moreNode];
                            }
                        }
                    }

                    //===============================>> More Member to display
                    if($dataLevel2->hasMorePages()){
                        $more = $dataLevel2->total()-$dataLevel2->perPage()*$dataLevel2->currentPage(); 
                        $moreNode = $rowLevel1->id.'-'.$dataLevel2->currentPage();
                        $nodes[] = [
                            'id'=>$moreNode, 
                            'label'=>'More '.$more, 
                            'image'=> env('FILE_URL').'/'.'public/icon/users.png'
                        ];
                        $edges[] = ['from'=>$rowLevel1->id, 'to'=>$moreNode];
                    }
                }
            }
            //===============================>> More Member to display
            if($dataLevel1->hasMorePages()){
                $more = $dataLevel1->total()-$dataLevel1->perPage()*$dataLevel1->currentPage(); 
                $moreNode = $nodeId.'-'.$dataLevel1->currentPage();
                $nodes[] = [
                    'id'=>$moreNode, 
                    'label'=>'More '.$more, 
                    'image'=> env('FILE_URL').'/'.'public/icon/users.png'
                ];
                $edges[] = ['from'=>$nodeId, 'to'=>$moreNode];
            }

        }
            
        return ['nodes'=> $nodes, 'edges'=>$edges];
    }

    public static function nodeInterface($node){
        
        
   
        $role = ""; 
        $label = $node->user->name; 
        if(isset($node->branch)){
            $role = 'សាខាខេត្ត '.$node->branch->name; 
            $label = $node->branch->name; 

        }elseif(isset($node->depot)){
            $role = 'ដេប៉ូស្រុក '.$node->depot->province->name .'-'. $node->depot->name; 
            $label =  $node->depot->name; 
        }else{
            $role = 'អ្នកលក់ចល័ត'; 
        }

      

        if($node->subscription){
            return [
                'id'            =>  $node->id, 
                'label'         =>  $label, 
                'uid'           =>  $node->user->uid, 
                'name'          =>  $node->user->name.' - '.$node->id, 
                'phone'         =>  $node->user->phone, 
                'image'         =>  Self::getPositionIcon($node), 
                'subscription'  =>  $node->subscription->name, 
                'role'          =>  $role, 
                'location'      => $node->province->name .' - '. $node->district->name
            ];
        }   
    }

    public static function getPositionIcon($member){
        //$data = json_decode($member->subscription->icon); 
        //return env('FILE_URL').'/public/icon/ppl.png'; 
        return env('FILE_URL').'/'.$member->subscription->icon; 
        
    }
  
}
