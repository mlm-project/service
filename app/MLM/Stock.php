<?php

namespace App\MLM;

use App\Model\Member\Main as Member;
use App\Model\Member\Stock as MemberStock;
use App\Model\Package\Main as MasterStock;
use App\Model\Package\Main as Package;

class Stock{

    // ==========================================================================>> Master Stock
    public static function getMasterStock($packageId = 0){
        $masterStock = MasterStock::find($packageId); 
        return $masterStock; 
    }

    public static function updateMasterStocks($type = 'stock-in',  $trx){

        $details = $trx->details; 
        foreach($details as $detail){
           
            $masterStock = Self::getMasterStock($detail->package_id); 

            if($type == 'stock-in'){
                $masterStock->qty = $masterStock->qty + $detail->qty; 

            }else{
                $masterStock->qty = $masterStock->qty - $detail->qty; 
            }
    
            $masterStock->save();
    
        }

    }

    
    // ==========================================================================>> Branch Stock
    public static function memberStocks($memberId = 0, $packageId = 0){
    
        $memberStock = memberStock::where([
            'member_id' => $memberId, 
            'package_id' => $packageId
        ])->first(); 

        if(!$memberStock){

            $memberStock = new memberStock; 
            $memberStock->member_id = $memberId; 
            $memberStock->package_id = $packageId; 
            $memberStock->qty = 0; 
            $memberStock->updated_at = now(); 
            $memberStock->save(); 
        }

        return $memberStock; 
    }
    
    public static function updateMemberStocks($type = 'stock-in',  $trx){

        $details = $trx->details; 
        foreach($details as $detail){
           
            $memberStock = Self::memberStocks($trx->member_id, $detail->package_id); 

            if($type == 'stock-in'){ // This Auction happens when Master Approves Branch's Purchased Request
                $memberStock->qty = $memberStock->qty + $detail->qty; 
               
                $masterStock            = Self::getMasterStock($detail->package_id); 
                $masterStock->qty       = $masterStock->qty + $detail->qty; // Deduct QTY for Master Stock
                $masterStock->save();

            }else{
                $memberStock->qty = $memberStock->qty - $detail->qty; 
            }
           
            $memberStock->save();
            // dd($memberStock);
            //Trx can be Purchase
            $detail->member_stock_id = $memberStock->id; 
            $detail->save(); 
    
        }

    }

    public static function updatePackageUnit($package){
        if($package && $package->is_single_product == 0){
            $products = $package->products; 
            $nOfUnits = 0; 
            if(sizeof($products) > 0){
                foreach($products as $product){
                    $nOfUnits += $product->qty; 
                }
            }
            $package->n_of_units =  $nOfUnits; 
            $package->save(); 
        }
    }

    // Master Stock
    public static function masterStocks( $packageId = 0){
    
        $masterStock = Package::find($packageId); 
        return $masterStock; 
    }

    // Update master stock
    public static function updateMasterStock($type="", $trx =0){
        $details = $trx->details; 
        foreach($details as $detail){
           
            $masterStock = Self::masterStocks($detail->package_id); 

            if($type == 'stock-in'){ // This Auction happens when Master Approves Branch's Purchased Request
               

            }else{
                $masterStock->qty = $masterStock->qty - $detail->qty; 
            }
    
            $masterStock->save();
    
        }
    }

    public static function updateMemberStockIn($type = 'stock-in',  $trx){

        $details = $trx->details; 
        foreach($details as $detail){
           
            $memberStock = Self::memberStocks($trx->member_id, $detail->package_id); 

            if($type == 'stock-in'){ // This Auction happens when Master Approves Branch's Purchased Request
                $memberStock->qty = $memberStock->qty + $detail->qty; 
                $memberStock->save();
            }
           
           
        }

    }
}
