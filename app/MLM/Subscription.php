<?php

namespace App\MLM;

use App\Model\Purchase\Main as Purchase;
use App\Model\Member\Subscription as MemberSubscription;
use App\Model\Subscription\Main as SubscriptionPackage;

class Subscription{

    public static function countPurchases($memberId, $from){
        $purchases = Purchase::select('id', 'total_price')
        ->where('member_id', $memberId)
        ->where('purchased_at', '>', $from)
        ->get(); 
        $nOfUnites = 0; 
        $totalPrice = 0; 

        foreach($purchases as $purchase){
            $totalPrice += $purchase->total_price; 
            foreach($purchase->details as $detail){
                $nOfUnites += $detail->qty; 
            }
        }

        return [
            'nOfUnits'      => $nOfUnites, 
            'totalPrice'    => $totalPrice
        ]; 
    }

   

    public static function checkSubscriptionStatus($purchase){
        
        if($purchase->subscription){
           
            $totalUnits = $purchase->total_units; 

            if( $purchase->subscription->id >= $purchase->member->subscription->id ||  $purchase->member->subscription->id == 1 ){

                $purchase->member->subscription_id          =  $purchase->subscription->id; 
                $purchase->member->save(); 

                //Check for Branch
                if($purchase->member->branch){
                    if($purchase->subscription_id == 6){ // VVIP
                        $purchase->member->is_structure_active = 1; 
                        $purchase->member->structure_activated_at   = now(); 
                        $purchase->member->save(); 
                    }
                }

                //Check for Depot
                if($purchase->member->depot){
                    if( $purchase->subscription_id >= 5 ){ // At least VIP
                        $purchase->member->is_structure_active = 1; 
                        $purchase->member->structure_activated_at   = now(); 
                        $purchase->member->save(); 
                    }
                }

                self::createSubscriptionHistory($purchase,  $purchase->subscription->id); 
            }

                
           
            return $totalUnits; 
        }

        return 0; 
    }


    public static function createSubscriptionHistory($purchase, $subscriptionId = 0){

        $lateActiveSubscription           = MemberSubscription::select('*')
        ->where([
            'member_id' =>  $purchase->member_id, 
            'status_id' => 2
        ])
        ->orderBy('id', 'desc')
        ->first();

        if($lateActiveSubscription){
            $lateActiveSubscription->status_id = 5; // Terminate; 
            $lateActiveSubscription->save(); 
        }

        $subscription                       = new MemberSubscription; 
        $subscription->member_id            = $purchase->member_id; 
        $subscription->status_id            = 2; // Active 
        $subscription->subscription_id      = $subscriptionId; 
        $subscription->activated_at         = now(); 
        $subscription->expired_at           = date('Y-m-d 23:59:59', strtotime('+30 days', strtotime(Date('Y-m-d')))); 
        $subscription->save();

        $purchase->activation_id = $subscription->id; 
        $purchase->save(); 

       // $purchase->member->purchases()->where(['counted_at' => null])->update(['counted_at'=> now()]); 

    }

    


   
  
}
