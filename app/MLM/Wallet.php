<?php

namespace App\MLM;

use App\Model\Member\Main as Member;
use App\Model\User;
use App\Model\Transaction\USD as USDTrx;

class Wallet{
    
  
    public static function updateBalance(array $params = [], $currency = '', $type = 'active'){
        
        //Get member current wallet; 
        $wallet = Self::checkBalance($params['memberId'], $currency); 
        $balance = 0; 
        if($params['typeId'] == 1){
            $balance = $wallet-$params['amount'];
        }else{
            $balance = $wallet; 

            if( $type == 'active' ){
                $balance += $params['amount'];
            }
        }; 
        
        $trx                = new USDTrx; 
        $trx->type_id       = $params['typeId'];
        $trx->category_id   = $params['categoryId'];
        $trx->member_id     = $params['memberId'];
        
        if(isset($params['adminId'])){
            $trx->admin_id      = $params['adminId']; 
        }

        if(isset($params['orderId'])){
            $trx->order_id      = $params['orderId']; 
        }

        if(isset($params['purchaseId'])){
            $trx->purchase_id  = $params['purchaseId']; 
        }

        $trx->amount        = $params['amount'];
        $trx->balance       = $balance;
        $trx->description   = $params['description'];
        $trx->save();

        return $trx; 

    }

    public static function checkBalance($uid = 0){
        $member = Member::whereHas('user', function($query) use ($uid){
            $query->where('uid', $uid); 
        })
        ->first(); 

        if(!$member){
            $member = Member::find($uid); 
        }

        if($member){
            $trx = USDTrx::select('balance')->where('member_id', $member->id)->orderBy('id', 'DESC')->first(); 
            if($trx) return $trx->balance; else  return 0; 
        }else{
            return 0; 
        }
       

    }
  
}
