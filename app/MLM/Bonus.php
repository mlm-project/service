<?php

namespace App\MLM;

use App\Model\Member\Main as Member;
use App\Model\Package\Main as Package;
use App\MLM\Wallet; 

class Bonus{
    
    // =========================================================================>> Purchase
    public static function purchase($purchase){
       
        // Network Commision
        $bonuses['network']         = self::networkCommision($purchase->member->sponsor, $purchase, 1, 1, []); 
        
        // Depot Purchase
        $bonuses['depotPurchase']   = self::depotPurchase($purchase->member, $purchase, $purchase->total_units); 

        // Store Purchase
        $bonuses['storePurchase']   = self::storePurchase($purchase->member, $purchase);  

        return $bonuses; 

       
     
    }

    public static function networkCommision($member, $purchase,  $commisionLevel = 1, $networkLevel = 1, $trxs = []){
        if($commisionLevel <= 3){
            //Check if this member has subscription or not
            //return $member->subscription; 
            if($member){
                if($member->subscription){
                    
                    //Check if this member subscription is enigible for the level. 
                    $commision = self::getCommision($member->subscription->id, $commisionLevel); 
                    if($commision > 0){

                        $trxs[] = Wallet::updateBalance([
                            'categoryId'    => 2, // Network Bonus
                            'typeId'        => 2, // Received
                            'amount'        => $purchase->total_units*$commision, 
                            'memberId'      => $member->id,                            
                            'purchaseId'    => $purchase->id,
                            'description'   => 'L'.$commisionLevel.'-G'.$networkLevel.' from '.$purchase->member->user->uid. ' P#'.$purchase->receipt_number. ' U:'.$purchase->total_units . 'x'.$commision
                        ]); 

                        $commisionLevel++; 
                    }else{
                        $trxs[] = [
                            'uid' => $member->user->name, 
                            'cl'  => $commisionLevel, 
                            'sub' => $member->subscription->name
                        ]; 
                    }

                    $networkLevel++; 

                    return self::networkCommision($member->sponsor, $purchase, $commisionLevel, $networkLevel, $trxs); 
                   

                }
            }
           
        }
        return $trxs; 
    }

    public static function getCommision($subscriptionId = 0, $level = 1){
        $subscriptions = [
            '2' => [1], 
            '3' => [1, 2], 
            '4' => [1, 2, 3], 
            '5' => [1, 2, 3], 
            '6' => [1, 2, 3], 
        ]; 

        if(isset($subscriptions[$subscriptionId])){
            if(in_array($level, $subscriptions[$subscriptionId])){

                $commisitons = [
                    '1' => 0.5, 
                    '2' => 0.15, 
                    '3' => 0.1
                ]; 
    
                return ($commisitons[$level]) ? $commisitons[$level] : 0; 
            }
        }

        return 0; 
    }

    public static function depotPurchase($member, $purchase, $nOfUnits = 0){
        $trx = []; 
      
        if($member->depot_id){ // check if purchase account is a depot
            $branch = Member::where('is_structure_active', 1)
            ->find($member->branch_owner_id);
        
            if(!$branch){ // Default account will receive reword if branc is not active
                $branch     = self::getDefaultAccount(); 
            }

            $trx[] = Wallet::updateBalance([
                'categoryId'    => 3, // Depot Purchase
                'typeId'        => 2, // Received
                'amount'        => $nOfUnits*0.15, 
                'memberId'      => $branch->id, 
                'purchaseId'    => $purchase->id,
                'description'   => 'From '.$member->user->uid. ' #'.$purchase->receipt_number. ' U:'.$nOfUnits. ' C:0.15'
            ]);
            
        }
        return $trx;
    }

    public static function storePurchase($member, $purchase){
        $trx = []; 
      
        if(!$member->depot_id && !$member->branch_id){ // Not a depot or branch
            $nOfUnits = 0; 
            $defaultAccount     = self::getDefaultAccount(); 
            $useDefaultAccount  = false;

            // Finding Branch
            $branch = Member::where('is_structure_active', 1)
            ->where('branch_id', $member->province_id)
            ->first();

            if(!$branch){
                $branch             =   $defaultAccount;
                $useDefaultAccount  =   true; 
            }

            if($branch){

                // Get Packae that this branch could get.
                $branchAccessiblePackages           = Package::select('id'); 
                if( !$useDefaultAccount ){
                    $branchAccessiblePackages =  $branchAccessiblePackages->whereHas('branches', function($query) use ($branch){
                        $query->where('branch_id', $branch->id);   
                    }); 
                }
                $branchAccessiblePackages = $branchAccessiblePackages ->get(); 

                $purchaseDetails = $purchase->details; 
                foreach($purchaseDetails as $detail){
                    foreach($branchAccessiblePackages as $branchAccessiblePackage){
                        if($detail->package_id == $branchAccessiblePackage->id){
                            $nOfUnits += $detail->qty*$detail->n_of_units; 
                        }    
                    }
                }

                if($nOfUnits > 0){
                    $trx[] = Wallet::updateBalance([
                        'categoryId'    => 4, // Store Purchase
                        'typeId'        => 2, // Received
                        'amount'        => $nOfUnits*0.10, 
                        'memberId'      => $branch->id, 
                        'purchaseId'    => $purchase->id,
                        'description'   => 'From '.$member->user->uid. ' #'.$purchase->receipt_number. ' U:'.$nOfUnits. ' C:0.10'
                    ]);
                }
            }

            // Find Depot
            if($nOfUnits > 0){
                
                $depot = Member::where('is_structure_active', 1)
                ->where('depot_id', $member->district_id)
                ->first();

                if(!$depot){
                    $depot             =   $defaultAccount;
                }

                if($depot){
                    $trx[] = Wallet::updateBalance([
                        'categoryId'    => 4, // Store Purchase
                        'typeId'        => 2, // Received
                        'amount'        => $nOfUnits*0.25, 
                        'memberId'      =>  $depot->id, 
                        'purchaseId'    => $purchase->id,
                        'description'   => 'From '.$member->user->uid.  '#'.$purchase->receipt_number. ' U:'.$nOfUnits. ' C:0.25'
                    ]); 
                }
            }
        }

        return $trx;
    }
    
    // =========================================================================>> Order
    public static function order($order){
        // Find unit
        $nOfUnits = $order->total_units; 

        if($nOfUnits > 0){

            // Cross Border
            $bonuses = self::croseBorder($order->seller, $order->buyer, $order); 

            return $bonuses; 
        }
    }

    public static function croseBorder($seller, $customer, $order){

        // If seller is store , then auto transfer branch  (province), to related province & auto transfer depot (district) to related district
        // If seller is depot, them auto transfer branch (province) to related province & auto tranfer own wallet to related district
        // If seller is branch, them auto tranfer own wallet to both related province and district of the buyer

        $provincialPayers = []; 
        $districtPayers = []; 

        if($seller->branch_id){

            $provincialPayers    = [$seller]; 
            $districtPayers      = [$seller]; 
          
        }elseif($seller->depot_id){

            $provincialPayers    = [$seller->branchOwner]; 
            $districtPayers      = [$seller]; 

        }else{
            $defaultAccount     = self::getDefaultAccount(); 
            // Mobile do not need to pay. But their branch and store instead.
           
             // Store Sell to customer which branch of their own province, no payment. 
            $enableBranchPayment = true; 
            if($customer->branch_id){ 
                if($customer->branch_id == $seller->province_id){ // Same Province
                    $enableBranchPayment = false; 
                }
            }else{
                if($customer->province_id == $seller->province_id){ // Customer & Seller has same province
                    $enableBranchPayment = false; 
                }
            }

            if($enableBranchPayment){
                $provincialPayers  = Member::where('branch_id', $seller->province_id)->get();
                if(count($provincialPayers) == 0){
                    $provincialPayers      = [$defaultAccount]; 
                }
            }
            
             // Store Sell to customer which depot of their own district, no payment. 
            $enableDistrictPayment = true; 
            if($customer->depot_id){
                if($customer->depot_id == $seller->district_id){
                    $enableDistrictPayment = false; 
                }
            }

            if($enableDistrictPayment){
                $districtPayers    = Member::where('depot_id', $seller->district_id)->get(); 
                if(count($districtPayers) == 0){
                    $districtPayers      = [$defaultAccount]; 
                }
            }
            

        }

        $dataTrx = []; 
        if(count($provincialPayers) > 0 ){
            $dataTrx['province'] = self::tranferProvinceCrossBorder($seller, $customer, $order, $provincialPayers); 
        }

        if(count($districtPayers) > 0 ){
            $dataTrx['district'] = self::tranferDistrictCrossBorder($seller, $customer, $order, $districtPayers); 
        }
        

        return [
            'provincialPayers' => $provincialPayers, 
            'districtPayers' => $districtPayers, 
            'dataTrx' => $dataTrx
        ]; 
    }

    public static function tranferProvinceCrossBorder($seller, $customer, $order, $payers){
        
        $useDefaultAccount  = false;
        $trx = []; 
        $branches = []; 

        $defaultAccount     = self::getDefaultAccount();   // Get default account for receive payment if customer's district does not have depot yet
        $useDefaultAccount  = true; 
        foreach($payers as $payer){
            $branches[] = $defaultAccount; 
        }

         // Check if customer is a branch
         if ($customer->branch_id) {

            $canCalculate = true; 
            // Depot make order to its own branch must not have transaction
            if( $seller->depot_id  ){
                if($seller->branch_owner_id == $customer->id ){
                    $canCalculate = false; 
                    $branches = []; //One of Branches received

                }
            }

            if($canCalculate){
                if ($customer->is_structure_active == 1) {
                    $useDefaultAccount  = false;
                    $branches = Member::where('branch_id', $customer->branch_id)->get();
                }
            }

        }elseif($customer->depot_id){
          
            if ($customer->is_structure_active == 1) {
                $useDefaultAccount  = false;
                $branches = Member::where('province_id', $customer->province_id)->get();
            }
         
        }else{

            // Finding branches that need to receive money. 
            
            $nOfBranches = $customer->province->branches; 
            if(count($nOfBranches) > 0){
                $useDefaultAccount  = false; 
                $branches = $nOfBranches; 
            }
            
        }
       
        foreach($branches as $branch){
            foreach($payers as $payer){
                if($branch->id != $payer->id){

                    // Get Packae that this branch could get.
                    $branchAccessiblePackages           = Package::select('id')
                    ->whereHas('products'); 
                    
                    if( !$useDefaultAccount ){
                        $branchAccessiblePackages =  $branchAccessiblePackages->whereHas('branches', function($query) use ($branch){
                            $query->where('branch_id', $branch->id);   
                        }); 
                    }

                    $branchAccessiblePackages =  $branchAccessiblePackages->get(); 

                    // Get Packae that this Payer could get.
                    $payerAccessiblePackages           = Package::select('id')
                    ->whereHas('products')
                    ->whereHas('branches', function($query) use ($payer){
                        $query->where('branch_id', $payer->id);   
                      
                    })
                    ->get(); 
 
                    $nOfUnits = 0; 
                    $orderDetails = $order->details; 

                    foreach($orderDetails as $detail){
                        foreach($branchAccessiblePackages as $branchAccessiblePackage){
                            foreach($payerAccessiblePackages as $payerAccessiblePackage){
                                if($detail->package_id == $branchAccessiblePackage->id && $detail->package_id == $payerAccessiblePackage->id){
                                    $nOfUnits += $detail->qty*$detail->n_of_units; 
                                }
                            }
                               
                        }
                    }

                    if($nOfUnits > 0 ){

                        $receiver = $branch; 
                        if($branch->is_structure_active == 0){
                            $receiver = $defaultAccount; 
                        }


                        $trx[] = Wallet::updateBalance([
                            'categoryId'    => 7, // Province Cross Border Pay
                            'typeId'        => 1, // Sent
                            'amount'        => $nOfUnits*0.05, 
                            'memberId'      => $payer->id, 
                            'orderId'       => $order->id,
                            'description'   => 'To '.$receiver->user->uid. ' #'.$order->receipt_number. ' U:'.$nOfUnits. ' S:'.$seller->user->uid
                        ]);

                        $trx[] = Wallet::updateBalance([
                            'categoryId'    => 5, // Province Cross Border Sale
                            'typeId'        => 2, // Received
                            'amount'        => $nOfUnits*0.05, 
                            'memberId'      => $receiver->id, 
                            'orderId'       => $order->id,
                            'description'   => 'From '.$payer->user->uid. ' #'.$order->receipt_number. ' U:'.$nOfUnits
                        ]);   
                    } 
                }
            }
               
        }

        return [
            'branches' => $branches, 
            'trx' => $trx
        ]; 
      
    }

    public static function tranferDistrictCrossBorder($seller, $customer, $order, $payers){
        $trx        = []; 
        $depots     = []; 
       
        $defaultAccount     = self::getDefaultAccount();   // Get default account for receive payment if customer's district does not have depot yet
        $useDefaultAccount  = true; 
        foreach($payers as $payer){
            $depots[] = $defaultAccount; 
        }
       
        // Check if customer is a depo or branch
        if($customer->branch_id || $customer->depot_id){
            if($customer->depot_id){
                if($customer->is_structure_active == 1){
                    $depots = Member::where('depot_id', $customer->depot_id)->get();
                }
            }else{
                $depots = [];  // Becase customer is a branch; No Depot crose border
            }
           
        }else{

            $nOfDepots = $customer->district->depots; 
            if(count($nOfDepots) > 0){
                $useDefaultAccount  = false; 
                $depots =  $customer->district->depots; 
            }
            
        }

 
        foreach($depots as $depot){
            foreach($payers as $payer){
                if($depot->id != $payer->id){

                    // Get Packae that this depot could get.
                    $branchAccessiblePackages           = Package::select('id')
                    ->whereHas('products'); 

                    if( !$useDefaultAccount ){
                        $branchAccessiblePackages =  $branchAccessiblePackages->whereHas('branches', function($query) use ($depot){
                            $query->where('branch_id', $depot->branch_owner_id);   
                        }); 
                    }

                    $branchAccessiblePackages =  $branchAccessiblePackages->get(); 

                    // Get Packae that this Payer could get.
                    $payerAccessiblePackages           = Package::select('id')
                    ->whereHas('products')
                    ->whereHas('branches', function($query) use ($payer){
                        if($payer->branch){
                            $query->where('branch_id', $payer->id);   
                        }else{
                            $query->where('branch_id', $payer->branch_owner_id);   
                        }
                    })
                    ->get(); 

                    
                    $nOfUnits = 0; 
                    $orderDetails = $order->details; 
                  
                    foreach($orderDetails as $detail){
                        foreach($branchAccessiblePackages as $branchAccessiblePackage){
                            foreach($payerAccessiblePackages as $payerAccessiblePackage){
                                if($detail->package_id == $branchAccessiblePackage->id && $detail->package_id == $payerAccessiblePackage->id){
                                    $nOfUnits += $detail->qty*$detail->n_of_units; 
                                }
                            }    
                        }
                    }

                    if($nOfUnits > 0){

                        $receiver = $depot; 
                        if($depot->is_structure_active == 0){
                            $receiver = $defaultAccount; 
                        }

                        $trx[] = Wallet::updateBalance([
                            'categoryId'    => 8, // Depot Cross Border Pay
                            'typeId'        => 1, // Sent
                            'amount'        => $nOfUnits*0.10, 
                            'memberId'      => $payer->id, 
                            'orderId'       => $order->id,
                            'description'   => 'To '.$receiver->user->uid. ' #'.$order->receipt_number. ' U:'.$nOfUnits. 'S:'.$seller->user->uid
                        ]);

                        $trx[] = Wallet::updateBalance([
                            'categoryId'    => 6, // Depot Cross Border Sale
                            'typeId'        => 2, // Received
                            'amount'        => $nOfUnits*0.10, 
                            'memberId'      => $receiver->id, 
                            'orderId'       => $order->id,
                            'description'   => 'From '.$payer->user->uid. ' #'.$order->receipt_number. ' U:'.$nOfUnits. ' C:'.$customer->user->uid
                        ]);  
                       
                    }
                }
            } 
        }
        
        return [
            'depots' => $depots, 
            'trx' => $trx
        ]; 

    }

    private static function getDefaultAccount(){
        $defaultAccount = Member::find(1); //The top one;function
        return $defaultAccount; 
    }
   
  
}
