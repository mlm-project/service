<?php

namespace App\Api\V1\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use JWTAuth;
use Carbon\Carbon;
use App\Api\V1\Controllers\ApiController;
use App\Model\User\Main as User;
use App\Model\Member\Main as Member;
use App\CamCyber\FileUpload;
use App\Model\User\Code;
// use App\Model\Setup\Province;
// use App\Model\Setup\District;
use App\CamCyber\SMS;
use App\MLM\Account;
use App\MLM\Bot\BotRegister;

//========================== Use Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;
use Illuminate\Support\Facades\DB;

class RegisterController extends ApiController
{

    public function register(Request $request) {
      
        $this->validate($request, [
         
            'name'      => 'required|max:60',
           
            'phone'     =>  [
                            'required',  
                             Rule::unique('user', 'phone')
                        ],
            'email'     =>   [
                           
                            'email', 
                            'max:100', 
                            Rule::unique('user', 'email')
                        ],
            'password'  => 'required|min:6|max:60',
            'ref'       => 'required',
            // 'province_id' => 'required|exists:province,id',
            // 'district_id' => 'required|exists:district,id', 

        ], [

            'phone.unique'=>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.regex'=>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.required'=>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 

            // 'email.required'=>'E-mail is required.', 
            'email.unique'=>'អ៊ីមែលត្រូវបានយករួចហើយ។',
            
            // 'province_id.required' => 'សូមជ្រើសរើសខេត្ត។', 
            // 'province_id.exists' => 'សូមជ្រើសរើសខេត្តដែលមានសុពលភាព។', 

            // 'district_id.required' => 'សូមជ្រើសរើសស្រុក។', 
            // 'district_id.exists' => 'សូមជ្រើសរើសស្រុកដែលមានសុពលភាព។'
        ]);

        //Check for valid ref
        $ref = Member::whereHas('user', function($query) use ($request){
            $query->where('uid', $request->ref); 
        })
         ->whereHas('subscriptions')
        ->first(); 

        if($ref){

            DB::beginTransaction();

            try {

            // Check if submitted district is really in submitted province. 
            // $district = District::where([
            //     'province_id' => $request->province_id, 
            //     'id' => $request->district_id
            // ])->first(); 

            // if(!$district){
            //     return response()->json([
            //         'message' => 'ស្រុកមិនត្រឹមត្រូវ'
            //     ], 400); 
            // }


            //====================================>> Create New user
            $user = new User();
            //For proposal maker
            $user->type_id      = 2;
            $user->uid          = Account::generateUid();
            $user->name_kh      = $request->input('name_kh');
            $user->name         = $request->input('name');
            $user->phone        = $request->input('phone');
            $user->email        = $request->input('email');
            $user->is_active    = 0;
            $user->password     = bcrypt($request->input('password'));
            $user->save();

            $member = new Member;  
            $member->user_id      =  $user->id; 
            $member->sponsor_id   =  $ref->id; 
            $member->refferal_id  =  $ref->id; 
            // $member->subscription_id= 1;
            // $member->card         =  $request->input('card');
            
            $last = Member::select('id')->orderBy('id', 'DESC')->first();
            $id = 0;
            if($last){
                $id = $last->id+1;
            }

            $image = FileUpload::uploadImage($request, 'image', ['/uploads', '/idcard', '/'.$id, '/images'], [['xs', 200, 200]]);
            if($image != ""){
                $member->image = $image;
            }
            $image = FileUpload::resize($request, 'image', ['/uploads', '/idcard', '/'.$id, '/images'], [['xs', 200, 200]]);
            if($image != ""){
                $member->image = $image;
            }
            if($image != ""){
                $member->image = $image;
            }

            $member->save(); 

            $notification = $this->getSMSCode($request->input('phone'));
            
            //====================================>> Send Email Verify Code
            /// $notification = $this->getEmailCode($request->input('email'));
           
           $botRes = BotRegister::newRegister($member, 'Online', $notification['code']); 
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

            return response()->json([
                'status'        => 'success',
                'message'       => 'គណនីត្រូវបានបង្កើតដោយជោគជ័យ។', 
                'data'          => $user,
                'member'        => $member,  
                //'notification'  => $notification,
                'botRes' => $botRes
            
            ], 200); 
        }else{
            return response()->json([
                'message'       => 'ព័ត៌មានបញ្ជូនមិនត្រឹមត្រូវ។ សូមពិនិត្យម្តងទៀត។'
            ], 400); 
        }

          
    }

    public function getSMSCode($phone) {
        
        $user = User::where(['phone'=>$phone,'deleted_at'=>null])->first(); 
       
        if($user){
            $code = new Code; 
            $code->user_id = $user->id; 
            $code->code = rand(10000000, 999999);
            $code->type = 'VERIFY';
            $code->is_verified = 0; 
            $code->save(); 

            $notification = [
                'name'      => $user->name,
                'code'      => $code->code,
            ];

            // $sms = SMS::sendSMS($user->phone, 'Please use this code :'.$code->code.' to verify your request. Thanks!');
            $sms = Code ::select($user->phone, 'Please use this code :'.$code->code.' to verify your request. Thanks!');

            return $notification;
        }   

    }

    public function getEmailCode($email) {
        
        $user = User::where(['email'=>$email, 'deleted_at'=>null])->first(); 
       
        if($user){

            $code = new Code; 
            $code->user_id = $user->id; 
            $code->code = rand(10000000, 999999);
            $code->type = 'VERIFY';
            $code->is_verified = 0; 
            $code->save(); 

            $notification = [
                'name'      => $user->name,
                'code'      => $code->code,
            ];
            // $notification       = [
            //     'username'      => $user->username,
            //     'member'        => $user->name,
            //     'email'         => $user->email,
            //     'phone'         => $user->phone,
            //     'country'       => $user->country->name
            // ];
            Mail::to($user->email)->send(new Notification('Welcome New Member', $notification, 'emails.member.account.verify-email'));

            return $notification;
        }   

    }
    public function getTelegramCode($phone) {
        
        // $dotenv = Dotenv\Dotenv::create(__DIR__);

        // $dotenv->load();
        $user = User::where(['phone'=>$phone, 'deleted_at'=>null])->first(); 

        if ($user) {
        
        //    $message= ('Please use this code');
           $token   = getenv( "TELEGRAM_BOT_TOKEN" );
           $chatID  = getenv( "REGISTER_CHANNEL_CHAT_ID" );

           $code = new Code; 
           $code->user_id = $user->id; 
           $code->code = rand(100000, 999999);
           $code->type = 'VERIFY';
           $code->is_verified = 0; 
           $code->save(); 

           $data = [
            
            //    'message'    => $message,  
               'code'     => $code->code,                           
           ];
        
         $file = file_get_contents( "http://api.telegram.org/bot$token/sendMessage?" . http_build_query( $data ) );
        };

    }
  
    public function verifyAccount(Request $request) {
        
        $this->validate($request, [
            'username'  => 'required',
            'code'      => 'required|min:7',
        ]);
        
        $code = $request->post('code'); 

        $data = Code::where(['code'=>$code, 'type'=>'VERIFY'])->orderBy('id', 'DESC')->first(); 
        $totalMinutesDifferent = 0;
        if($data){
            //Check if expired
            $created_at = Carbon::parse($data->created_at);
            $now = Carbon::now(env('APP_TIMEZONE')); 
            $totalMinutesDifferent = $now->diffInMinutes($created_at);

            if($totalMinutesDifferent < 30){
                $user = User::findOrFail($data->user_id);
                if($user){
                    
                    //Updated Code
                    $code = Code::find($data->id); 
                    if($code->is_verified == 0){

                        $code->is_verified = 1; 
                        $code->verified_at = now(); 

                        $code->save(); 

                        $user->is_active = 1;

                        if(filter_var($request->post('username'), FILTER_VALIDATE_EMAIL)){
                           
                            if($user->email){
                                $user->is_email_verified = 1; 
                                $user->email_verified_at = now();
                            }
                        } else{
                            if($user->phone){    
                                $user->is_phone_verified = 1; 
                                $user->phone_verified_at = now();
                                //Send Notification
                                // $sms = SMS::sendSMS($user->phone, 'សូមអរគុណសម្រាប់ការចុះឈ្មោះរបស់អ្នក។ លេខសម្គាល់អ្នកប្រើរបស់អ្នកគឺ'.$user->uid.'. សូមរីករាយជាមួយការវិនិយោគ!');
                                $sms = Code::select($user->phone, 'សូមអរគុណសម្រាប់ការចុះឈ្មោះរបស់អ្នក។ លេខសម្គាល់អ្នកប្រើរបស់អ្នកគឺ'.$user->uid.'. សូមរីករាយជាមួយការវិនិយោគ!');

                            }
                        }
                        $user->save();
                        //Crate token
                        $token = JWTAuth::fromUser($user);

                        $botRes = BotRegister::registerVerify($user->member, $request->post('code')); 
                        
                        return response()->json([
                            'status'=> 'success',
                            'token'=> $token, 
                            'botRes' => $botRes
                        ], 200);
                    }else{
                         return response()->json([
                            'status'=> 'error',
                            'message'=> 'លេខកូដបានបញ្ជាក់រួចហើយ' 
                        ], 200);
                    }
                        


                }else{
                     return response()->json([
                        'status'=> 'error',
                        'message'=> 'រកមិនឃើញអ្នកប្រើប្រាស់' 
                    ], 200);
                }
            }else{
                return response()->json([
                    'status'=> 'error',
                    'message'=> 'លេខកូដផុតកំណត់'
                ], 200);
            }
                
        }else{
            return response()->json([
                'status'=> 'error',
                'message'=> 'លេខកូដមិនត្រឹមត្រូវ' 
            ], 200);
        }
    }

    public function requestverifyCode(Request $request){
        $user = User::where('phone',$request->username)->orWhere('email', $request->username)->first();
        if($user){

            if(filter_var($request->post('username'), FILTER_VALIDATE_EMAIL)){
                $notification = $this->getEmailCode($request->input('username'));  
                return response()->json([
                    'status'        => 'success',
                    'message'       => 'លេខកូដត្រូវបានផ្ញើឡើងវិញ', 
                    'notification'  => $notification,
                ], 200);     
              
            } else{
               $notification = $this->getSMSCode($request->input('username'));
               return response()->json([
                'status'        => 'success',
                'message'       => 'លេខកូដត្រូវបានផ្ញើឡើងវិញ', 
                'notification'  => $notification,
            ], 200);   
            }
        }else{
            return response()->json([
                'status'=> 'error',
                'message'=> 'ឈ្មោះអ្នកប្រើមិនត្រឹមត្រូវ' 
            ], 404);
        }
        

    }

}
