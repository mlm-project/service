<?php

namespace App\Api\V1\Controllers\Auth;

use Illuminate\Http\Request;
use App\Api\V1\Controllers\ApiController;
use JWTAuth;

class LoginController extends ApiController
{

    public function login(Request $request) {

        
        $this->validate($request, [
            'username'    =>  [
                            'required',
                        ],
            'password' => 'required|min:6|max:20',
            'type' => 'required',
        ],[
            'username.required'=>'Username is required.', 
            'password.required'=>'Password is required.', 
            'password.min'=>'Password must be at least 6 digit long.', 
            'password.max'=>'Password is not allowed to be longer than 20 digit long.', 
        ]); 
        
        
        $type = 3; // member
        if($request->type == 'admin'){
            $type = 1; 
        }elseif($request->type == 'master' or $request->type == 'admin'){
            $type = 1; 
            $type = 2;
           
        }

        if(filter_var($request->post('username'), FILTER_VALIDATE_EMAIL)){
            $credentails = array(
                'email'=>$request->post('username'), 
                'is_email_verified'=>1,
                'password'=>$request->post('password'), 
                //'is_active'=>1, 
                'type_id'=>$type, 
                'deleted_at'=>null,
            );
        }else{

            $credentails = array(
                'phone'             =>  $request->post('username'), 
                'is_phone_verified' =>  1,
                'password'          =>  $request->post('password'), 
               // 'is_active'         =>  1, 
                'type_id'           =>  $type, 
                'deleted_at'        =>  null,
            );
        }
       
        

        try{

            $token = JWTAuth::attempt($credentails);
            
            if(!$token){
               
                return response()->json([
                    'status'=> 'error',
                    'message' => 'ឈ្មោះអ្នកប្រើឬពាក្យសម្ងាត់មិនត្រឹមត្រូវ។'
                ], 401);
            }

        } catch(JWTException $e){
            return response()->json([
                'status'=> 'error',
                'message' => 'មិនអាចបង្កើតនិមិត្តសញ្ញាទេ!'
            ], 500);
        }

        $user = JWTAuth::toUser($token);

        if($user->is_active != 1){
             return response()->json([
                 'status'=> 'error',
                 'message' => 'គណនីរបស់លោកអ្នកត្រូវបានផ្អាកដំណើរការ! សូមទាក់ទងទៅកាត់ក្រុមហ៊ុន MGMT'
             ], 401);
        }

        $user =['name' => $user->name, 'phone' => $user->phone, 'type' => $user->type_id, 'avatar' => $user->avatar];

        return response()->json([
            'status'=> 'success',
            'token'=> $token, 
            'user' => $user,

        ], 200);
    }
}
