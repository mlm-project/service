<?php

namespace App\Api\V1\Controllers\CP\Package;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Api\V1\Controllers\ApiController;
use App\Model\Package\PackageProduct as Main;
use App\Model\Package\Product;
use App\Model\Package\Main as Package;

use App\MGTB\Stock; 
// Import Resource
use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;

class ProductController extends ApiController
{
    use Helpers;

  
    function listing($packageId = 0 ){

       
        $data = Product::select('id', 'sku', 'category_id', 'en_name', 'kh_name','image')
        ->whereHas('info', function($query) use ($packageId){
            $query->where('package_id', $packageId)
            ->whereHas('package', function($query){
                $query->where('is_single_product', 0); 
            }); 
        })
        ->with(['info' => function($query) use ($packageId){
            $query->where('package_id', $packageId)
            ->whereHas('package', function($query){
                $query->where('is_single_product', 0); 
            }); 
        }])
       
        ->get();

      
       

        $data = new PackagesCollection($data);
        return response()->json($data, 200);
    }

    function updateQTY(Request $request, $packageId = 0){

        $this->validate($request, [
            'product' => 'required|exists:product,id', 
            'qty'     => 'required'
        ],[
            'product.required'         => 'សូមបញ្ចូលផលិតផល',
            'product.exists'           => 'មិនមានផលិតផល',  

            'qty.required'           => 'សូមបញ្ចូល QTY',  
        ]);

        //Check if Package is valid
        $data = Main::where([
            'package_id' => $packageId, 
            'product_id' => $request->product
        ])->first(); 
        
        
        if($data){

            $data->qty = $request->qty; 
            $data->save();

            //Update Number of Units
            Stock::updatePackageUnit($data->package); 

            return response()->json([
                'status' => 'success',
                'message' => 'QTY has been updated',
                'res' => $data
            ], 200);

           
        }else{
            
            return response()->json([
                'message' => 'Invalid record',
            ], 400);
        }

       
    }

    // Add or Remove
    function action($packageId = 0, $productId = 0){
       
        //Check if Package is valid
        $package = Package::where([
            'id' => $packageId, 
            'is_single_product' => 0
        ])->first();

        

        if($package){
             //Check if Product really existed in the package
             $check = Main::where([
                'package_id' => $packageId, 
                'product_id' => $productId
            ])->first(); 

            if($check){

                //Remove
                $check->delete(); 

                // Update Number of Units
                Stock::updatePackageUnit($check->package); 

                return response()->json([
                    'status' => 'success',
                    'message' => 'ផលិតផលត្រូវបានយកចេញ',
                ], 200);
            }else{

                $data = new Main; 
                $data->package_id =  $packageId; 
                $data->product_id =  $productId; 
                $data->save(); 

                // Update Number of Units
                Stock::updatePackageUnit($data->package); 

                return response()->json([
                    'message' => 'ផលិតផលត្រូវបានបន្ថែម',
                    'data' => $data
                ], 200);
            }

        }else{
            return response()->json([
                'message' => 'កញ្ចប់មិនត្រឹមត្រូវ',
            ], 400);
        }

      
    }

    function availableProducts($packageId = 0){

      $data = Product::select('id', 'sku', 'en_name', 'kh_name', 'image')
      ->whereDoesntHave('info', function($query) use ($packageId){
          $query->where('package_id', $packageId); 
      })
      ->get();  
     $data = new PackagesCollection($data);
      return response()->json($data, 200);
    }

}
