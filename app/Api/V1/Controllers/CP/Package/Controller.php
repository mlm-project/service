<?php

namespace App\Api\V1\Controllers\CP\Package;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Package\Main as Package;
use App\Model\Package\Product;
use App\Model\Package\PackageProduct;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

// Import Resource
use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;

class Controller extends ApiController
{
    use Helpers;
    function listing(Request $req) {

        $data = Package::select('id', 'sku', 'kh_name', 'en_name', 'selling_price', 'cost_price', 'discount', 'qty', 'n_of_units', 'image' ,'updated_at')
        ->with([
            'prices', 
            'products'
        ]) 
        ->withCount([
            'products as n_of_products',
        ])
        ->where([
            'is_single_product' => 0
        ]);

         // ==============================>> Key Search
        if($req->key && $req->key != "") {
            $data = $data->where(function ($query) use ($req) {
                $query->where('kh_name', 'like', '%' . $req->key . '%');
            });
        }

        // ==============================>> Date Range
        if($req->from && $req->to && isValidDate($req->from) && isValidDate($req->to)){
            $data = $data->whereBetween('created_at', [$req->from." 00:00:00", $req->to." 23:59:59"]);
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        $data = new PackagesCollection($data);
        return $data;
    }

    function view($id = 0){

        $data   = Package::select('*')
        ->find($id); 

        if($data){
            $data = new PackageResource($data);
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 400);
        }
        
    }

    function create(Request $req)
    {
       
        $this->validate($req, [
            'sku'               =>  [
                'required', 
                Rule::unique('package', 'sku')
            ],
            'kh_name'           => 'required|max:100',
            'en_name'           => 'required|max:100',
            'cost_price'        => 'required|numeric',
            'selling_price'     => 'required|numeric',
            'discount'          => 'required|numeric'
        ], [

            'sku.unique'               => 'This sku number has already been takend. Please try another one.',
            'sku.required'             => 'Please enter package sku.',
            'sku.max'                  => 'Package sku must not be more then 20 chacters.',

            'kh_name.required'          => 'Please enter name in khmer .',
            'kh_name.max'               => 'Name must not be more then 100 chacters.',

            'en_name.required'          => 'Please enter name in english.',
            'en_name.max'               => 'Name must not be more then 100 chacters.',

            'cost_price.required'       => 'Please enter cost price.',
            'cost_price.numeric'        => 'Cost price must be number.',

            'selling_price.required'    => 'Please enter selling price.',
            'selling_price.numeric'     => 'Selling price must be number.',

            'discount.required'         => 'Please enter discount rate.',
            'discount.numeric'          => 'Discount rate must be number.'
        ]);

        $data                       = new Package();
        $data->sku                  = $req->sku;
        $data->kh_name              = $req->input('kh_name');
        $data->en_name              = $req->input('en_name');
        $data->cost_price           = $req->input('cost_price');
        $data->selling_price        = $req->input('selling_price');
        $data->discount             = $req->input('discount');
        $data->description          = $req->input('description');

        //Need to create folder before storing images
    
        $image = FileUpload::uploadImage($req, 'image', ['uploads', '/packages','/' .uniqid(), '/image'], [['xs', 200, 200]]);
        if ($image != "") {
            $data->image = $image;
        }

        $data->save();
        
        // Add Price
        $this->generatePrices($data->id); 

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }

    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'sku'              => 'required|max:20',
            'kh_name'           => 'required|max:100',
            'en_name'           => 'required|max:100',
            'cost_price'        => 'required|numeric',
            'selling_price'     => 'required|numeric',
            'discount'          => 'required|numeric'
        ], [
            'sku.required'             => 'Please enter package sku.',
            'sku.max'                  => 'Package sku must not be more then 20 chacters.',

            'kh_name.required'          => 'Please enter name in khmer .',
            'kh_name.max'               => 'Name must not be more then 100 chacters.',

            'en_name.required'          => 'Please enter name in english.',
            'en_name.max'               => 'Name must not be more then 100 chacters.',

            'cost_price.required'   => 'Plese enter cost price.',
            'cost_price.numeric'    => 'Cost price must be number.',

            'selling_price.required'    => 'Plese enter selling price.',
            'selling_price.numeric'     => 'Selling price must be number.',

            'discount.required'    => 'Plese enter discount rate.',
            'discount.numeric'     => 'Discount rate must be number.'
        ]);

        $data   = Package::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
          
            $data->sku                 = $req->sku;
            $data->kh_name              = $req->input('kh_name');
            $data->en_name              = $req->input('en_name');
            $data->cost_price           = $req->input('cost_price');
            $data->selling_price        = $req->input('selling_price');
            $data->discount             = $req->input('discount');
            $data->description          = $req->input('description');

            $image = FileUpload::uploadImage($req, 'image', ['uploads', '/packages','/' .uniqid(), '/image'], [['xs', 200, 200]]);
            if ($image != "") {
                $data->image = $image;
            }
            $data->save();
            
      
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }

       
    }
    
    function delete($id=0){

        $package = Package::where([
            'is_single_product' => 0
        ])->find($id); 
        
        if($package){
            
            PackageProduct::where([
                'package_id' => $id
            ])->delete(); 

            $package->delete(); 
            return response()->json([
                'message' => 'កញ្ចប់ត្រូវបានលុបចោល។', 
            ], 200);
        }else{
            return response()->json([
                'message' => 'កញ្ចប់មិនត្រឹមត្រូវ', 
            ], 400);
        }
    }


    function generatePrices($packageId){
       
        DB::table('package_prices') -> insert(
            [
                [
                    'package_id' => $packageId,
                    'from' => 1,
                    'to' => 9,
                    'price' => 15
                ],
                [
                    'package_id' => $packageId,
                    'from' => 10,
                    'to' => 59,
                    'price' => 12
                ],
                [
                    'package_id' => $packageId,
                    'from' => 60,
                    'to' => 99,
                    'price' => 11
                ],
                [
                    'package_id' => $packageId,
                    'from' => 100,
                    'to' => 249,
                    'price' => 10
                ],
                [
                    'package_id' => $packageId,
                    'from' => 250,
                    'to' => 499,
                    'price' => 8.5
                ],
                [
                    'package_id' => $packageId,
                    'from' => 500,
                    'to' => 100000000,
                    'price' => 6.40
                ]
            ]
        );
    }

    
}
