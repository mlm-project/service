<?php

namespace App\Api\V1\Controllers\CP\Package;

use Illuminate\Http\Request;

use App\Api\V1\Controllers\ApiController;
use App\Model\Package\PackagePrice as PackagePrice;
use Dingo\Api\Routing\Helpers;
use JWTAuth;


class priceController extends ApiController
{
    use Helpers;
    function listing($id=0){

        $data = PackagePrice::select('id', 'from', 'to', 'price')
        ->where( 'package_id', $id)
        ->orderBy( 'id', 'desc')
        ->get();
        return response()->json($data, 200);
    }

   
    function updatePrice(Request $req, $packageId = 0){

        $this->validate($req, [
            'price'     => 'required'
        ],[ 
            'price.required'           => 'សូមបញ្ចូល price',  
        ]);

        //Check if Package is valid
        $packageprice = PackagePrice::find($req->range); 
        
        if($packageprice){

            $packageprice->price = $req->price; 
            $packageprice->save();

            return response()->json([
                'status' => 'success',
                'message' => 'តម្លៃត្រូវបានធ្វើបច្ចុប្បន្នភាព',
                'res' => $packageprice
            ], 200);

           
        }else{
            
            return response()->json([
                'message' => 'កំណត់ត្រាមិនត្រឹមត្រូវ',
            ], 400);
        }

       
    }



}
