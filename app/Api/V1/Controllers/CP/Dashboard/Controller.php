<?php

namespace App\Api\V1\Controllers\CP\Dashboard;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Main as Member;
use App\Model\Purchase\Main as Purchase;
use App\Model\Order\Main as Order;


class Controller extends ApiController
{
    use Helpers;
    function getRegister(){

        $today 		    = Carbon::today()->toDateTimeString('Y-m-d 00:00:00'); 
        $now 		    = Carbon::now()->toDateTimeString('Y-m-d h:i:s');
        $totalRegister 	= member::select('*')->count();
       

        $members     = Member::select('*')
        ->with([
            'user', 
            'subscription',
            'district',
            'province',
            'activeSubscription'
        ])
        ->withCount([
            'orders as n_of_orders', 
            'purchases as n_of_purchases'
        ])
        ->orderBy('id', 'desc')
        ->limit(10)
        ->orderBy('id', 'desc')->get();
        
        return response()->json([
            'totalRegister'=>$totalRegister,
            'data'=>$members
        ], 200);

    }

    function getSale(){
        
        $today      = Carbon::today()->toDateTimeString('Y-m-d 00:00:00'); 
        $now        = Carbon::now()->toDateTimeString('Y-m-d h:i:s');
        $totalSales = Order::select('total_price')->where('approved_at', '<>', null)->whereBetween('ordered_at', [$today, $now])->whereBetween('ordered_at', [$today, $now])->sum('total_price');

        // $totalSales = Order::select('total_price')->where('received_at', '<>', null)->whereBetween('ordered_at', [$today, $now])->whereBetween('ordered_at', [$today, $now])->sum('total_price');
        $sales      = Purchase::select('*')
        // ->where('received_at', '<>', null)
        ->with([
            'details', 
            'member',
        ])
        ->whereBetween('created_at', [$today, $now])
        ->limit(10)->get();
        
        return response()->json([
            'totalSales'=>$totalSales, 
            'data'=>$sales
        ], 200);
    }
  

}
