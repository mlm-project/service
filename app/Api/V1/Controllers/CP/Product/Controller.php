<?php

namespace App\Api\V1\Controllers\CP\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Package\Product;
use App\Model\Package\Main as Package;
use App\Model\Package\PackageProduct;

use App\Model\Event\Request as VenueRequest;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

// Import Resource
use App\Api\V1\Resources\Product\ProductResource;
use App\Api\V1\Resources\Product\ProductCollection;

use App\Api\V1\Resources\Package\PackageResource;
use App\Api\V1\Resources\Package\PackagesCollection;

class Controller extends ApiController
{
    use Helpers;
    function listing(Request $req){
        
        $data = Package::select('id', 'sku', 'kh_name', 'en_name', 'selling_price', 'cost_price', 'discount', 'qty', 'n_of_units', 'image' ,'updated_at')
        ->with([
            'prices', 
            'products'
        ]) 
        ->whereHas('products')
        ->where([
            'is_single_product' => 1
        ]); 

        // key search 
        if($req->key && $req->key != "") {
            $data = $data->where(function ($query) use ($req) {
                $query->where('kh_name', 'like', '%' . $req->key . '%')
                ->orWhere('en_name', 'like', '%' . $req->key . '%')
                ;
            });
        }
        
        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ?$req->limit:10);
        $data = new PackagesCollection($data);
        return $data;
    }

    function view($id = 0){
        
        $data   = Package::select('*')
        ->with(

            'products'
        )
        ->where([
            'is_single_product' => 1
        ])
        ->find($id);

        if($data){
            $data = new ProductResource($data);
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'Record not found. '.$id
            ], 404);
        }
    }

    function create(Request $req ,$id=0) {
        $admin_id = JWTAuth::parseToken()->authenticate()->id;

        $this->validate($req, [
           
            'sku'               =>  [
                'required', 
                Rule::unique('product', 'sku')
            ],
            'category_id'       => 'required',
            'kh_name'           => 'required|max:100',
            'kh_name'           => 'required|max:100',
            'en_name'           => 'required|max:100',
            'cost_price'        => 'required|numeric',
            'selling_price'     => 'required|numeric',
            'image'             => 'required|max:500',
        ], 
        [
            
            'sku.unique'               => 'This sku number has already been taken. Please try another one.',
            'sku.required'             => 'Please enter Product code.',
            'sku.max'                  => 'Product code must not be more then 20 chanters.',
            
            'category_id.required'      => 'Please select category .',

            'kh_name.required'          => 'Please enter name in khmer .',
            'kh_name.max'               => 'Name must not be more then 100 chanters.',

            'en_name.required'          => 'Please enter name in english.',
            'en_name.max'               => 'Name must not be more then 100 chanters.',

            'cost_price.required'       => 'Please enter cost price.',
            'cost_price.numeric'        => 'Cost price must be number.',

            'selling_price.required'    => 'Please enter selling price.',
            'selling_price.numeric'     => 'Selling price must be number.',
            
            'image.max'                     => 'Image Size Max 500 '
        ]);

        /** Save Product */
        $product                       = new Product();
        $product->category_id          = $req->input('category_id');
        $product->sku                  = $req->input('sku');
        $product->kh_name              = $req->input('kh_name');
        $product->en_name              = $req->input('en_name');
        $product->cost_price           = $req->input('cost_price');
        $product->selling_price        = $req->input('selling_price');
        $product->description          = $req->input('description');

        $image = FileUpload::uploadImage($req, 'image', ['uploads', '/products', '/'.uniqid(), '/images'], [['xs', 200, 200]]);
        if($image != ""){
            $product->image = $image;
        }
       
        $product->save();

        /** Save Package */
        $package                       = new Package();
        $package->sku                  = $req->input('sku');
        $package->kh_name              = $req->input('kh_name');
        $package->en_name              = $req->input('en_name');
        $package->cost_price           = $req->input('cost_price');
        $package->selling_price        = $req->input('selling_price');
        $package->n_of_units           =1;
        $package->description          = $req->input('description');
        $package->image                = $image != "" ? $image : $package->image ;
       
        $package->is_single_product = 1;
      
        $package->save();

        // Add Price
        //$this->generateProductPrices($product->id); 
        $this->generatePackagePrices($package->id); 
          
        /** Product Pakcage */

        $productPackage             = new PackageProduct(); 
        $productPackage->package_id = $package->id; 
        $productPackage->product_id = $product->id; 
        $productPackage->save(); 
        
        return response()->json([
            'status'            => 'success',
            'message'           => 'បានបង្កើតដោយជោគជ័យ',
            'product'           => $product,
            'package'           => $package,
            'productPackage'    => $productPackage,

        ], 200);
    }

    function update(Request $req, $id=0){

        $this->validate($req, [

            'sku'               => 'required|max:20',
            'kh_name'           => 'required|max:100',
            'en_name'           => 'required|max:100',
            'cost_price'        => 'required|numeric',
            'selling_price'     => 'required|numeric',
            'category_id'       => 'required',
          
        ], [
            'category_id.required'      => 'Please select category .',

            'sku.required'              => 'Please enter package code.',
            'sku.max'                   => 'Package code must not be more then 20 chanters.',

            'kh_name.required'          => 'Please enter name in khmer .',
            'kh_name.max'               => 'Name must not be more then 100 chanters.',

            'en_name.required'          => 'Please enter name in english.',
            'en_name.max'               => 'Name must not be more then 100 chanters.',

            'cost_price.required'       => 'Please enter cost price.',
            'cost_price.numeric'        => 'Cost price must be number.',

            'selling_price.required'    => 'Please enter selling price.',
            'selling_price.numeric'     => 'Selling price must be number.'
        ]);

        //========================================================>>>> Start to update
        

        /** update  Product */
        $product                       = Product::find($id);
        $product->category_id          = $req->input('category_id');
        $product->sku                  = $req->input('sku');
        $product->kh_name              = $req->input('kh_name');
        $product->en_name              = $req->input('en_name');
        
        //Need to create folder before storing images
        $image = FileUpload::uploadImage($req, 'image', ['uploads', '/products', '/'.uniqid(), '/images'], [['xs', 200, 200]]);
        if($image != ""){
            $product->image = $image;
        }

        $product->save();

            /** update  Package */
        $package                       = Package::find($id);
        $package->sku                  = $req->input('sku');;
        $package->kh_name              = $req->input('kh_name');
        $package->en_name              = $req->input('en_name');
        $package->cost_price           = $req->input('cost_price');
        $package->selling_price        = $req->input('selling_price');
        $package->description          = $req->input('description');
        $package->image                 = $image != "" ? $image : $package->image ;    
        
        $package->save();
     
            
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'package' => $package,
                'product' => $product,
            ], 200);
    }

    function delete($id=0){

        $product = Product::with([
            'info' => function($query){
                $query->whereHas('package', function($query){
                    $query->where('is_single_product', 1); 
                }); 
            }
        ])->find($id); 

        if($product){
           
            if($product->info->package_id){
                    $package = Package::find($product->info->package_id); 
                    if($package){
                        $package->delete(); 
                        $product->info->delete();
                        $product->delete();
                    }
                    return response()->json([
                        'message' => 'ផលិតផលត្រូវបានលុបចោល។', 
                    ], 200);

            }else{
                return response()->json([
                    'message' => 'ផលិតផលមិនត្រឹមត្រូវ', 
                ], 400);
            }

           
        }else{
            return response()->json([
                'message' => 'ផលិតផលមិនត្រឹមត្រូវ', 
            ], 400);
        }
    }

    function generateProductPrices($productId){

        $priceRanges =  [
            [
                'product_id' => $productId,
                'from' => 1,
                'to' => 9,
                'price' => 10
            ],
            [
                'product_id' => $productId,
                'from' => 10,
                'to' => 59,
                'price' => 7
            ],
            [
                'product_id' => $productId,
                'from' => 60,
                'to' => 99,
                'price' => 6.50
            ],
            [
                'product_id' => $productId,
                'from' => 100,
                'to' => 249,
                'price' => 6
            ],
            [
                'product_id' => $productId,
                'from' => 250,
                'to' => 499,
                'price' => 5
            ],
            [
                'product_id' => $productId,
                'from' => 500,
                'to' => 100000000,
                'price' => 4
            ]
        ]; 

        // Product Price RangePrice Range
        DB::table('product_prices') -> insert($priceRanges);
    }

    function generatePackagePrices($packageId){

        $priceRanges =  [
            [
                'package_id' => $packageId,
                'from' => 1,
                'to' => 9,
                'price' => 10
            ],
            [
                'package_id' => $packageId,
                'from' => 10,
                'to' => 59,
                'price' => 7
            ],
            [
                'package_id' => $packageId,
                'from' => 60,
                'to' => 99,
                'price' => 6.50
            ],
            [
                'package_id' => $packageId,
                'from' => 100,
                'to' => 249,
                'price' => 6
            ],
            [
                'package_id' => $packageId,
                'from' => 250,
                'to' => 499,
                'price' => 5
            ],
            [
                'package_id' => $packageId,
                'from' => 500,
                'to' => 100000000,
                'price' => 4
            ]
        ]; 
       
        DB::table('package_prices') -> insert($priceRanges);
    }
   
}
