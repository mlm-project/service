<?php

namespace App\Api\V1\Controllers\CP\Package;

use Illuminate\Http\Request;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Package\Main as Package;
use App\Model\Stock\Detail as Main;
use Dingo\Api\Routing\Helpers;
use JWTAuth;


class StockController extends ApiController
{
    use Helpers;
    function listing(Request $req,$id=0){

        $data = Main::select('*')
        ->where( 'package_id', $id)
        ->With(
            'package'
            )
        ->orderBy( 'id', 'desc');

     // key search API
     $limit      =   intval(isset($_GET['limit']) ? $_GET['limit'] : 10);
     $key       =   isset($_GET['key']) ? $_GET['key'] : "";

     if ($key != "") {
         $data = $data->where(function ($query) use ($key) {
             $query->where('name', 'like', '%' . $key . '%');
         });
         $appends['key'] = $key;
     }

        // Key search Date API
        $from = isset($_GET['from']) ? $_GET['from'] : "";
        $to = isset($_GET['to']) ? $_GET['to'] : "";
        if (isValidDate($from)) {
            if (isValidDate($to)) {

                $appends['from'] = $from;
                $appends['to'] = $to;

                $from .= " 00:00:00";
                $to .= " 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        $data = $data->orderBy('id', 'desc')->paginate( $req->limit ? $req->limit : 10);
        return response()->json($data, 200);
    }
    
}
