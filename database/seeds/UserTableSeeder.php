<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       


        DB::table('users_type')->insert(
            [
                ['name' => 'admin'],
                ['name' => 'user']
                
            ]
        );

        // ============================================================ MGTB Admin
        $userId = DB::table('user')->insertGetId([ 
            'type_id'=>1, 
            'email'=>'admin.mlm@gamil.com',                   
            'phone' => '010000001', 
            'password' => bcrypt('123456'), 
            'is_active'=>1, 
            'is_email_verified'=>1, 
            'is_phone_verified'=>1,
            'name' => 'MLM',
            'uid' => 'MLM000001'
        ]);

       
    }
}
