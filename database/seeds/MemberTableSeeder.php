<?php

use Illuminate\Database\Seeder;
use App\MLM\Account; 

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
        for($i = 1; $i <= 1; $i++){

            // ============================================================ Branch
            $userId = DB::table('user')->insertGetId([ 
                'type_id'           =>  2, 
                'uid'               =>  Account::generateUid(), 
                'email'             =>  'branch-'.$i.'@mlm.com',                   
                'phone'             =>  '02000000'.$i, 
                'password'          =>  bcrypt('123456'), 
                'is_active'         =>  1, 
                'is_email_verified' =>  1, 
                'is_phone_verified' =>  1,
                'name_kh' => 'បូ ពិសី',
                'name' => 'Bo Pisey', 
               
               
            ]);

            $memberAsBranchId = DB::table('member')->insertGetId([ 
                'sponsor_id'   => $i-1, 
                'user_id'       => $userId, 
            ]);

           

        }
        

        
	}
}
