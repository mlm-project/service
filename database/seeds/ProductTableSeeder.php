<?php

use Illuminate\ Database\ Seeder;

class ProductTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public
    function run() {
        DB::table('products_category') -> insert(
            [
                ['name' => 'ឡេលាបមុខ'], // 1
                ['name' => 'ឡេលាបក្លៀក'], // 2
                ['name' => 'ជែលលាងមុខ'], // 3
                ['name' => 'សេរ៉ូម'],
                ['name' => 'សាប៊ូកក់សក់'],
                ['name' => 'សាប៊ូអប់សក់'],
                ['name' => 'ប្រេងអប់សក់'],
                ['name' => 'សាប៊ូលាងខ្លួន'],
                ['name' => 'ឡូឆាន់'],
                ['name' => 'ទឹកអនាម័យស្ដ្រី'],
                ['name' => 'ម្សៅ'],
            ]
        );

        
    }
}