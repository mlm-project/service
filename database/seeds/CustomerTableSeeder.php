<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
       
        for($i = 1; $i <= 99; $i++){

            // ============================================================ User
            $userId = DB::table('user')->insertGetId([ 
                'type_id'           =>  2, 
                'email'             =>  'customer-'.$i.'@mlm.com',                   
                'phone'             =>  '04000000'.$i, 
                'password'          =>  bcrypt('123456'), 
                'is_active'         =>  1, 
                'is_email_verified' =>  1, 
                'is_phone_verified' =>  1,
                'name' => 'mlm '.$i
            ]);

            $memberId = DB::table('member')->insertGetId([ 'user_id' =>$userId, 'type_id'=> rand(1, 4)]);

            

        }
        

        
	}
}
