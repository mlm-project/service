<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('sponsor_id')->unsigned()->index()->default(1);
            $table->integer('refferal_id')->unsigned()->index()->default(1);
            $table->integer('role_id')->unsigned()->index()->default(1);          

            $table->string('address', 200)->nullable();
            $table->string('card', 200)->nullable();
            $table->string('image', 250)->default('');

            // Branch & Depot Structure
           
            $table->integer('branch_id')->unsigned()->index()->nullable(); // Provice that this member hold
            $table->integer('branch_owner_id')->unsigned()->index()->nullable(); // Member ID that this depot belong to (must be branch)
            $table->integer('depot_id')->unsigned()->index()->nullable(); // District that this member hold
            $table->integer('store_owner_id')->unsigned()->index()->nullable(); // Member ID that this store belong to (Could be branch & Store)
            $table->boolean('is_structure_active')->default(0);
            $table->dateTime('structure_activated_at')->nullable();

            $table->date('contract_started')->nullable();
            $table->date('contract_expired')->nullable();

            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
