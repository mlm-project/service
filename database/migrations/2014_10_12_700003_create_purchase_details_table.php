<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_details', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('purchase_id')->unsigned()->index()->nullable();
            $table->foreign('purchase_id')->references('id')->on('purchase')->onDelete('cascade');

            $table->integer('package_id')->unsigned()->index()->nullable();
            $table->foreign('package_id')->references('id')->on('package');
            
            $table->decimal('unit_price', 10, 2)->nullable();
            $table->integer('qty')->unsigned()->index()->nullable();
            $table->decimal('discount', 10, 2)->nullable();
            $table->integer('n_of_units')->unsigned()->default(0);
            $table->string('note', 250)->nullable();

            $table->integer('member_stock_id')->unsigned()->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_details');
    }
}
