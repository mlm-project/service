<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('receipt_number')->unsigned()->index()->nullable();

            $table->integer('member_id')->unsigned()->index()->nullable();
            $table->integer('buyer_id')->unsigned()->index()->nullable();
            //$table->foreign('buyer_id')->references('id')->on('member')->onDelete('cascade');
            
            $table->dateTime('ordered_at')->nullable();
            $table->dateTime('paid_at')->nullable();

            $table->integer('approver_id')->unsigned()->index()->nullable(); //Staff who has right to check and approve
            $table->dateTime('approved_at')->nullable();
            $table->mediumText('approved_note')->nullable();

            $table->integer('rejecter_id')->unsigned()->index()->nullable(); //Staff who has right to check and reject
            $table->dateTime('rejected_at')->nullable();
            $table->mediumText('rejected_note')->nullable();
           
            $table->decimal('total_price', 10, 2)->default(0); //USD
            $table->decimal('discount', 10, 2)->default(0); //%
            $table->integer('total_units')->unsigned()->default(0);
            $table->mediumText('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
