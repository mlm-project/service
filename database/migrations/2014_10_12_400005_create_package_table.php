<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package', function (Blueprint $table) {
            $table->increments('id', 11);

            //$table->integer('category_id')->unsigned()->index()->nullable();
            //$table->foreign('category_id')->references('id')->on('packages_category')->onDelete('cascade');

            $table->string('sku', 150)->nullable();
            $table->string('kh_name', 150)->nullable();
            $table->string('en_name', 150)->nullable();
            $table->string('image', 150)->nullable();

            $table->decimal('cost_price', 10, 2)->default(0);
            $table->decimal('selling_price', 10, 2)->default(0);
            $table->decimal('discount', 10, 2)->default(0);
           
            $table->mediumText('description')->nullable();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_single_product')->default(0);
            $table->integer('qty')->default(0); //Aviable in Stock
            $table->integer('n_of_units')->default(0); // Total Products in this package



            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package');
    }
}
