<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    
    //:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Making Auth
    $api->group(['prefix' => 'auth'], function ($api) {
        require(__DIR__.'/Api/V1/Auth/main.php');
    });
    //:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
    $api->group(['middleware' => ['api.auth', 'cors']], function($api) {

        $api->group(['prefix' => 'cp', 'middleware' => []], function ($api) {
            require(__DIR__.'/Api/V1/CP/main.php');
        });

    });

});
