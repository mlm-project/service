<?php

	
	$api->group(['namespace' => 'App\Api\V1\Controllers\CP'], function($api) {
		
		$api->group(['prefix' => 'dashboard'], function ($api) {
			require(__DIR__.'/dashboard.php');
		});

		$api->group(['prefix' => 'products'], function ($api) {
			require(__DIR__.'/product.php');
		});

		$api->group(['prefix' => 'packages'], function ($api) {
			require(__DIR__.'/package.php');
		});

		$api->group(['prefix' => 'setups'], function ($api) {
			require(__DIR__.'/setup.php');
		});
	
		$api->group(['prefix' => 'users'], function ($api) {
			require(__DIR__.'/user.php');
		});
	
		$api->group(['prefix' => 'my-profiles'], function ($api) {
			require(__DIR__.'/my-profile.php');
		});

	});