<?php

$controller = 'Package\Controller@';

$api->get('/', 				['uses' => $controller.'listing']);
$api->get('/{id}', 			['uses' => $controller.'view']);
$api->put('/{id}', 			['uses' => $controller.'update']); //Update
$api->post('/', 			['uses' => $controller.'create']); //Add new
$api->delete('/{id}', 		['uses'     => $controller.'delete']);

//////////////////////////// Product

$ProductController = 'Package\ProductController@';
$api->get('/{packageId}/avaiable-products', 				    ['uses' => $ProductController.'availableProducts']); // Products that has not been in this pakcage
$api->get('/{packageId}/products', 						        ['uses' => $ProductController.'listing']); // Added Products for this package
$api->put('/{packageId}/products', 						        ['uses' => $ProductController.'updateQTY']);// Update Product QTY
$api->get('/{packageId}/products/{productId}', 	                ['uses' => $ProductController.'action']); // Add or Remove product

//////////Stock Packages

$StockController = 'Package\StockController@';
$api->get('/{id}/stocks', 				            ['uses' => $StockController.'listing']);
// ===================== create images

$PackageImageController = 'Package\PackageImageController@';
$api->get('/{id}/images', 				            ['uses' => $PackageImageController.'listing']);
$api->post('/{id}/images', 			                ['uses' => $PackageImageController.'create']); 
$api->delete('/{id}/images/{package_images_id}',    ['uses' => $PackageImageController.'delete']); 

/////////// Package Prices

$PriceController = 'Package\PriceController@';
$api->get('/{id}/prices', 				            ['uses' => $PriceController.'listing']);
$api->post('/{id}/prices', 			                ['uses' => $PriceController.'create']); 
$api->put('/{packageId}/prices', 				    ['uses' => $PriceController.'updatePrice']);// Update 
$api->delete('/{id}/prices/{package_prices_id}',    ['uses' => $PriceController.'delete']); 